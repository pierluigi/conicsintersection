# README #

Given the homogeneous matrices of two conics it recovers the (up to) four intersection points

### What is this repository for? ###

* The offical c++ header only porting of the matlab code found at [matworks](http://www.mathworks.com/matlabcentral/fileexchange/28318-conics-intersection)
* 0.0.0.1


### How do I get set up? ###

* You will need to have Eigen3 installed. Then, simply pick the conicsIntersection.h file and put it into your project. 
* You might want to compile the provided tests. To do so you will need cmake

Ro use the code put your two conics to be intersected into two 3x3 symmetric matrix (Eigen::Matrix).
Then create a ConicIntersection instante and call intersect():


```
#!c++
ConicsIntersection<double> cint;
ConicsIntersection<double>::Conic c1, c2;
std::vector<ConicsIntersection<double>::Points> intersections;


// fill the two conics c1 and c2
//[...]


intersections = cint.intersect(c1,c2);

std::cout << "intersections are: "<< std::endl;
for (auto p : points){
  std::cout << p.head<2>() / p(2) << std::endl; //remember to normalize homogenous points
  std::cout << "   p C1 p = " << p.transpose() * c1 * p << std::endl;
  std::cout << "   p C2 p = " << p.transpose() * c2* p << std::endl;
}


```
### Other languages ###

I wrote a simple dll wrapper (libConicsIntersection) that can be used as a Cm interface for other languages.

#### C# wrapper ####
The sample c# project shows how to use the dll interface into any c# prjoject. Have a look to [sampleCsharp/main.cs](https://bitbucket.org/pierluigi/conicsintersection/src/0a583296e54579a00878789b03518bf14c1ea38c/sampleCsharp/?at=master). I contains a 64bit version of the dll. If you need it on other architecture you will need to build an import a different dll

### Who do I talk to? ###

* Pierluigi Taddei pierluigi.taddei@gmail.com