# this is a customized version for boost, do not use in other projects.

get_filename_component(fullFolderName "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY)
get_filename_component(realPath ${fullFolderName} REALPATH)

get_filename_component(PACKAGE_VERSION "${realPath}" NAME)


if("${PACKAGE_VERSION}" VERSION_LESS "${PACKAGE_FIND_VERSION}")
    set(PACKAGE_VERSION_COMPATIBLE FALSE)
MESSAGE(comp ver ${PACKAGE_VERSION})

elseif(IS_DIRECTORY "${fullFolderName}/lib")
    set(PACKAGE_VERSION_COMPATIBLE TRUE)
    if("${PACKAGE_FIND_VERSION}" STREQUAL "${PACKAGE_VERSION}")
        set(PACKAGE_VERSION_EXACT TRUE)
    endif()
else()
    set(PACKAGE_VERSION_UNSUITABLE TRUE)
endif()
