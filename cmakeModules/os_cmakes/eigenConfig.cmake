get_filename_component(fullFolderName "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY)
get_filename_component(realPath ${fullFolderName} REALPATH)

get_filename_component(PACKAGE_VERSION "${realPath}" NAME)


if (CMAKE_VERSION VERSION_LESS 3.0.0)
    message(FATAL_ERROR "${PACKAGE_NAME} requires at least CMake version 3.0.0")
endif()


get_filename_component(_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}" ABSOLUTE)


macro(_check_file_exists file)
    if(NOT EXISTS "${file}" )
        message(FATAL_ERROR "The imported target \”EIGEN\” references the file
   \"${file}\"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   \"${CMAKE_CURRENT_LIST_FILE}\"
but not all the files it references.
")
    endif()
endmacro()


set(EIGEN_INCLUDE_DIRS ${_INSTALL_PREFIX}/include/eigen3)
set(EIGEN_LIBRARIES ${PACKAGE_NAME})


_check_file_exists(${EIGEN_INCLUDE_DIRS})
include_directories(${EIGEN_INCLUDE_DIRS})
   
set(EIGEN_FOUND TRUE)
	