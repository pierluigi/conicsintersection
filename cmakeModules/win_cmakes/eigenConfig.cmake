get_filename_component(fullFolderName "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY)
get_filename_component(folderName "${fullFolderName}" NAME)
string(REGEX REPLACE "([^-]*)-.*$" "\\1" PACKAGE_NAME ${folderName})
string(TOUPPER ${PACKAGE_NAME} UPPERCASE_PACKAGE_NAME)
string(REGEX REPLACE "[^-]*-(.*)$" "\\1" PACKAGE_VERSION ${folderName})

if (CMAKE_VERSION VERSION_LESS 3.0.0)
    message(FATAL_ERROR "${PACKAGE_NAME} requires at least CMake version 3.0.0")
endif()


get_filename_component(_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}" ABSOLUTE)


macro(_check_file_exists file)
    if(NOT EXISTS "${file}" )
        message(FATAL_ERROR "The imported target \"${PACKAGE_NAME}\" references the file
   \"${file}\"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   \"${CMAKE_CURRENT_LIST_FILE}\"
but not all the files it references.
")
    endif()
endmacro()


set(${PACKAGE_NAME}_INCLUDE_DIRS "${_INSTALL_PREFIX}")
set(${PACKAGE_NAME}_LIBRARIES ${PACKAGE_NAME})


_check_file_exists(${${PACKAGE_NAME}_INCLUDE_DIRS})
include_directories(${${PACKAGE_NAME}_INCLUDE_DIRS})
	
   
set(${PACKAGE_NAME}_FOUND TRUE)
	