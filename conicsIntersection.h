// This file is part of ConicIntersection, a headr-only template library
// for conics intersections.
//
// Copyright (C) 2015 Pierluigi Taddei <pierluigi.taddei@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#pragma once

#include <Eigen/Dense>
#include <unsupported/Eigen/Polynomials>

#include <vector>
#include <tuple>

#include <iostream>

namespace Eigen{ //we extend eigen by adding conic intersection functionalities

  /*! utility class to perform intersection of conics obtaining up to four possible intersection 2D points in homogeneous coordinates*/
  template<class Real>
  class ConicsIntersection{

  public:

    using Conic = Matrix<Real, 3, 3>; //a conic in homogeneous coordinates is defined using a 3x3 matrix (make sure it is symmetric)
    using Point = Matrix<Real, 3, 1>; //a point in homogeneous coordinates is defined using a 3x1 matrix
    using Line =  Matrix<Real, 1, 3>; //a line in homogeneous coordinates is defined using a 1x3 matrix

    using ConicRef = Ref<const Conic>;
    using PointRef = Ref<const Point>;
    using LineRef  = Ref<const Line>;

    ConicsIntersection() = default;
    ~ConicsIntersection() = default;


    /*! intersects the two provided conics and store the intersection points in the pointBuffer provided.  The provided matrices must be symmetric. Returns the actual number of intersections (up to 4)*/
    int intersect(const ConicRef& a, const ConicRef& b, Real* pointBuffer);


    /*! intersects the two provided conics and return a vector containing the four possible intersection point. The provided matrices must be symmetric*/
    std::vector<Point> intersect(const ConicRef& a, const ConicRef& b){
      std::vector<Point> buffer(4);
      int c = intersect(a, b, (Real*)buffer.data());

      if (c < 4) buffer.resize(c);
      return buffer;
    }

    /*! intersect a conic c with a given line in homogeneous coordinates and return the (up to two) intersection */
    std::vector<Point> intersectLine(const ConicRef& c, const LineRef& l);

    /*! decompose a degenerate conic into the two lines that define it. This version accept the conic rank as input so that it do not have to estimate it again.
    It returns a boolean stating if the decomposition is valid and the two decomposed lines*/
    std::tuple<bool, Line, Line> decomposeDegenerateConic(const ConicRef& de, int rank);

    /*! decompose a degenerate conic into the two lines that define it. Will estimate the conic rank.
    It returns a boolean stating if the decomposition is valid and the two decomposed lines*/
    std::tuple<bool, Line, Line> decomposeDegenerateConic(const ConicRef& de);


    Matrix<Real, 3, 3> adjointSym(const Matrix<Real, 3, 3>& M);


  private:

    int completeIntersection(const ConicRef& e1, const ConicRef& e2, Real* pointBuffer);

    int degenerateIntersection(const ConicRef& fullE, const ConicRef& degenerateE, Real* pointBuffer);
    int degenerateIntersection(const ConicRef& fullE, const ConicRef& degenerateE, int degenerateRank, Real* pointBuffer);

    std::pair<Point, Point> pointsOnLine(const LineRef& l);

    Matrix<Real, 3, 3> crossMatrix(const PointRef& p);

  };


  template<class Real>
  std::vector<typename ConicsIntersection<Real>::Point> ConicsIntersection<Real>::intersectLine(const typename ConicsIntersection<Real>::ConicRef& C, const typename ConicsIntersection<Real>::LineRef& l){

    assert(C.cols() == 3 && C.rows() == 3 && l.cols() == 3 && l.rows() == 1);


    std::vector<Point> pts;

    Point p1, p2; //parametrize l as l =  p1 + k*p2
    Real k1, k2; //the coefficent of the intersection given the p1 p2 parametrization

    if (l(0) == 0 && l(1) == 0) return pts; // no intersection if line is at infinity

    std::tie(p1, p2) = pointsOnLine(l);

    //p1'Cp1 + 2k p1'Cp2 + k ^ 2 p2'Cp2 = 0
    Real p1Cp1 = p1.transpose() * C * p1;
    Real p2Cp2 = p2.transpose() * C * p2;
    Real p1Cp2 = p1.transpose() * C * p2;

    if (p2Cp2 == 0)
    { // linear
      k1 = -0.5*p1Cp1 / p1Cp2;
      pts.push_back(p1 + k1*p2);
    }
    else{
      Real delta = p1Cp2 * p1Cp2 - p1Cp1*p2Cp2;
      if (delta >= 0){
        Real deltaSqrt = std::sqrt(delta);
        k1 = (-p1Cp2 + deltaSqrt) / p2Cp2;
        k2 = (-p1Cp2 - deltaSqrt) / p2Cp2;

        pts.push_back(p1 + k1*p2);
        pts.push_back(p1 + k2*p2);
      }
      else {
        //no intersection at all.

      }
    }
    return pts;
  }


  template<class Real>
  int ConicsIntersection<Real>::intersect(const typename ConicsIntersection<Real>::ConicRef& E1, const typename  ConicsIntersection<Real>::ConicRef& E2, Real* pointBuffer)
  {
    assert(E2.cols() == 3 && E2.rows() == 3 && E1.cols() == 3 && E1.rows() == 3);

    //make sure you got symmetric matrices
    assert(E1(0, 1) == E1(1, 0) && E1(0, 2) == E1(2, 0) && E1(1, 2) == E1(2, 1));
    assert(E2(0, 1) == E2(1, 0) && E2(0, 2) == E2(2, 0) && E2(1, 2) == E2(2, 1));


    //estimate ranks
    Eigen::FullPivLU<Conic> lu1(E1);
    Eigen::FullPivLU<Conic> lu2(E2);

    auto r1 = lu1.rank();
    auto r2 = lu2.rank();

    if (r1 == 3 && r2 == 3){ //full rank case. Let's perform a complete intersection
      return completeIntersection(E1, E2, pointBuffer);
    }
    else if (r2 < 3){ // E2 is degenerate
      return degenerateIntersection(E1, E2, r2, pointBuffer);
    }
    else { //E1 is degenerate (or both)
      return degenerateIntersection(E2, E1, r1, pointBuffer);
    }
  }


  template<class Real>
  std::pair<typename ConicsIntersection<Real>::Point, typename ConicsIntersection<Real>::Point> ConicsIntersection<Real>::pointsOnLine(const typename ConicsIntersection<Real>::LineRef& l)
  {
    Point p1, p2;
    assert(l(0) != 0 || l(1) != 0); //line at infinity. TODO fix this test

    p2 << -l(1), l(0), 0;

    if (std::abs(l(0)) < std::abs(l(1))){
      p1 << 0, -l(2), l(1);
    }
    else {
      p1 << -l(2), 0, l(0);

    }
    return std::make_pair(p1, p2);
  }


  template<class Real>
  int ConicsIntersection<Real>::completeIntersection(const typename ConicsIntersection<Real>::ConicRef& E1, const typename  ConicsIntersection<Real>::ConicRef& E2, Real* pointBuffer){

    //solve the characteristic polynom : E1*(-E2) ^ -1 - lambda I
    //we exploit determinant multilinearity.
    //  k = [det(E1); ...
    //           det([E1(:, 1), E1(:, 2), E2(:, 3)]) + det([E1(:, 1), E2(:, 2), E1(:, 3)]) + det([E2(:, 1), E1(:, 2), E1(:, 3)]); ...
    //          det([E1(:, 1), E2(:, 2), E2(:, 3)]) + det([E2(:, 1), E1(:, 2), E2(:, 3)]) + det([E2(:, 1), E2(:, 2), E1(:, 3)]); ...
    //           det(E2); ...
    //];


    Matrix<Real, 3, 3> EE = E1 * (-E2).inverse();

    auto detEE12 = EE(0, 0)*EE(1, 1) - EE(0, 1)*EE(1, 0);
    auto detEE23 = EE(1, 1)*EE(2, 2) - EE(1, 2)*EE(2, 1);
    auto detEE13 = EE(0, 0)*EE(2, 2) - EE(0, 2)*EE(2, 0);


    Matrix<Real, 4, 1> k; //polynomial coefficents of the characteristic polynom (see for example doi:10.1016/j.aml.2005.07.007)
    /*k <<	-1,
        EE.trace(),
        -(detEE12 + detEE23 + detEE13),
        EE.determinant();*/
    k << EE.determinant(),
      -(detEE12 + detEE23 + detEE13),
      EE.trace(),
      -1;

    Eigen::PolynomialSolver<Real, 3> solver;
    solver.compute(k);
    bool realSolutions;
    auto largestRealRoots = solver.greatestRealRoot(realSolutions);

    if (!realSolutions){
      //no intersecting lines detected
      return 0;
    }
    else //there is a degenerate conics in the pencil. Decompose it
    {
      Line m, l;
      auto E0 = E1 + largestRealRoots*E2;
      //std::tie(m, l) = decomposeDegenerateConic(E0);
      auto output = decomposeDegenerateConic(E0);

      if (!std::get<0>(output)) {
        return 0;
      }
      m = std::get<1>(output);
      l = std::get<2>(output);

      auto P1 = intersectLine(E1, m);
      auto P2 = intersectLine(E1, l);

      int count = 0;
      for (auto& p : P1){
        Map<Point> outp(&pointBuffer[3 * count++]);
        outp = p;
      }

      for (auto& p : P2){
        Map<Point> outp(&pointBuffer[3 * count++]);
        outp = p;
      }

      return count;
    }

  }


  template<class Real>
  int ConicsIntersection<Real>::degenerateIntersection(const typename  ConicsIntersection<Real>::ConicRef& fullE, const typename  ConicsIntersection<Real>::ConicRef& defE, int rankD, Real* pointBuffer){
    Line m, l;
    bool valid;

    std::tie(valid, m, l) = decomposeDegenerateConic(defE, rankD);
    if (!valid)
    {
      return 0;
    }

    auto P1 = intersect(fullE, m);
    auto P2 = intersect(fullE, l);

    int count = 0;
    for (auto& p : P1){
      Map<Point> outp(&pointBuffer[count++]);
      outp = p;
    }

    for (auto& p : P2){
      Map<Point> outp(&pointBuffer[count++]);
      outp = p;
    }
    return count;
  }


  template<class Real>
  int ConicsIntersection<Real>::degenerateIntersection(const typename  ConicsIntersection<Real>::ConicRef& fullE, const typename  ConicsIntersection<Real>::ConicRef& defE, Real* pointBuffer){
    Line m, l;
    bool valid;

    std::tie(valid, m, l) = decomposeDegenerateConic(defE, rankD);
    if (!valid)
    {
      return 0;
    }

    auto P1 = intersect(fullE, m);
    auto P2 = intersect(fullE, l);

    int c = 0;
    for (auto& p : P1) pointBuffer[c++] = p;
    for (auto& p : P2) pointBuffer[c++] = p;

    return c;
  }


  template<class Real>
  std::tuple<bool, typename ConicsIntersection<Real>::Line, typename ConicsIntersection<Real>::Line> ConicsIntersection<Real>::decomposeDegenerateConic(const typename ConicsIntersection<Real>::ConicRef& de){

    Eigen::FullPivLU<Conic> lu(de);
    return decomposeDegenerateConic(de, lu.rank());
  }

  template<class Real>
  std::tuple<bool, typename ConicsIntersection<Real>::Line, typename ConicsIntersection<Real>::Line> ConicsIntersection<Real>::decomposeDegenerateConic(const typename ConicsIntersection<Real>::ConicRef& de, int dRank){

    assert(std::abs(de.determinant()) < 0.0001);

    Line l1, l2;

    Conic C;

    if (dRank == 1){ //de is rank 1: direct split is possible
      C = de;
    }
    else { //rank 2: need to detect the correct rank 1 matrix
      // use the dual conic of de
      auto B = -adjointSym(de);

      // detect intersection point p
      // get the greatest diagonal element;
      Eigen::Matrix<Real, 3, 1> diagonal = B.diagonal().array().abs();
      Real v = 0;
      int idx = 0;
      for (int i = 0; i < 3; i++)
      {
        if (v < diagonal(i))
        {
          v = diagonal(i);
          idx = i;
        }
      }

      assert(std::abs(B(idx, idx)) > 0);

      if (B(idx, idx) < 0)
      { //no intersection point present
        Line l;
        Line m;
        return std::make_tuple(false, l, m);
      }

      auto b = std::sqrt(v);
      auto p = B.col(idx) / b;

      // detect lines product
      auto Mp = crossMatrix(p);

      C = de + Mp;
    }

    // recover lines

    //first get the biggest element in the matrix
    Real currentMax = 0;
    int maxI, maxJ;

    for (int j = 0; j < 3; j++){
      for (int i = 0; i < 3; i++){
        if (currentMax < std::abs(C(i, j)))
        {
          maxI = i; maxJ = j;
          currentMax = std::abs(C(i, j));
        }
      }
    }

    auto l = C.row(maxI);
    auto m = C.col(maxJ).transpose();


    //return std::make_pair(l, m);
    return std::make_tuple(true, l, m);
  }


  template<class Real>
  Matrix<Real, 3, 3> ConicsIntersection<Real>::adjointSym(const Matrix<Real, 3, 3>& M)
  {
    Matrix<Real, 3, 3> A;
    auto a = M(0, 0); auto b = M(0, 1); auto d = M(0, 2);
    auto c = M(1, 1); auto e = M(1, 2);
    auto f = M(2, 2);

    A(0, 0) = c*f - e*e;
    A(0, 1) = -b*f + e*d;
    A(0, 2) = b*e - c*d;

    A(1, 0) = A(0, 1);
    A(1, 1) = a*f - d*d;
    A(1, 2) = -a*e + b*d;

    A(2, 0) = A(0, 2);
    A(2, 1) = A(1, 2);
    A(2, 2) = a*c - b*b;

    return A;
  }

  template<class Real>
  Matrix<Real, 3, 3>  ConicsIntersection<Real>::crossMatrix(const typename ConicsIntersection<Real>::PointRef& p){

    Matrix<Real, 3, 3> Mp = Matrix<Real, 3, 3>::Zero();
    Mp(0, 1) = p(2);
    Mp(0, 2) = -p(1);
    Mp(1, 0) = -p(2);
    Mp(1, 2) = p(0);
    Mp(2, 0) = p(1);
    Mp(2, 1) = -p(0);

    return Mp;
  }

} //Eigen namespace
