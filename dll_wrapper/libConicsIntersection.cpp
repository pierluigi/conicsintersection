// This file is part of ConicIntersection, a headr-only template library
// for conics intersections.
//
// Copyright (C) 2015 Pierluigi Taddei <pierluigi.taddei@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <conicsIntersection.h>
#include "libConicsIntersection.h"

using namespace Eigen;


int CALL intersectConics_F( const float* c1,  const float* c2,  float* points){

    ConicsIntersection<float> cint;

    Eigen::Map<const ConicsIntersection<float>::Conic> mc1(c1);
    Eigen::Map<const ConicsIntersection<float>::Conic> mc2(c2);
    
    return cint.intersect(mc1, mc2, points);
}

int CALL intersectConics_D(const double* c1, const double* c2, double* points){
    
    ConicsIntersection<double> cint;
    
	Eigen::Map<const ConicsIntersection<double>::Conic> mc1(c1);
	Eigen::Map<const ConicsIntersection<double>::Conic> mc2(c2);
    
    return cint.intersect(mc1, mc2, points);


}


