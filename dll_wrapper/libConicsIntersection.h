// This file is part of ConicIntersection, a headr-only template library
// for conics intersections.
//
// Copyright (C) 2015 Pierluigi Taddei <pierluigi.taddei@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.


#pragma once

#ifdef WIN32
    #ifdef libConicsIntersection_EXPORTS
        #define libConicsIntersection_API __declspec(dllexport)
    #else
        #define libConicsIntersection_API __declspec(dllimport)
    #endif
    #define CALL __stdcall
#else
    #define libConicsIntersection_API
    #define CALL
#endif

extern "C" libConicsIntersection_API int CALL intersectConics_F( const float* c1,  const float* c2,  float* points);

extern "C" libConicsIntersection_API int CALL intersectConics_D(const double* c1, const double* c2, double* points);

