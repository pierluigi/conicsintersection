#include "conicsIntersection.h"
#include <iostream>

using namespace Eigen;

int main(int argc, char** argv ){

    std::cout << "sample intersection of conics" << std::endl;
    
    ConicsIntersection<double> cint;
    
    ConicsIntersection<double>::Conic c1;
    c1 <<	1,  0,	 0,
    0,  0,	-0.5,
    0, -0.5, 0;
    ConicsIntersection<double>::Conic c2;
    c2 <<	3,  0, 0,
    0,  0, -0.5,
    0, -0.5, -2;
    
    auto points = cint.intersect(c1, c2);
    
    std::cout << "c1:" << std::endl;
    std::cout << c1 << std::endl;

    std::cout << "c2:" << std::endl;
    std::cout << c2 << std::endl;
    
    std::cout << "intersections are: "<< std::endl;
    
    

    for (auto p : points){
        std::cout << p.head<2>() / p(2) << std::endl;
        std::cout << "   p C1 p = " << p.transpose() * c1 * p << std::endl;
        std::cout << "   p C2 p = " << p.transpose() * c2* p << std::endl;

    }
}
