using System;
using System.Runtime.InteropServices;     // DLL support

namespace conicsIntersection{


class CSharpSample
{
    //make sure to compile the c sharp project using the correct architecture (x86 or x64)
    [DllImport("libConicsIntersection.dll", CallingConvention = CallingConvention.StdCall)]
    public static extern int intersectConics_D(double[] c1, double[] c2, double[] points);
      
    [DllImport("libConicsIntersection.dll", CallingConvention = CallingConvention.StdCall)]
    public static extern int intersectConics_F(float[] c1, float[] c2, float[] points);

    
    static void Main(string[] args)
    {

        double[] c1 = new double[9]{  1,  0,	 0,
                                    0,  0,	-0.5f,
                                    0, -0.5f, 0};
        double[] c2 = new double[9]{  3,  0, 0,
                                    0,  0, -0.5f,
                                    0, -0.5f, -2};

        double[] points = new double[12];
        
        int count = intersectConics_D(c1, c2, points);
    
        Console.WriteLine("c1:");
        for (int i = 0; i < 3; i++) { 
            for (int j = 0; j < 3; j++)   Console.Write((c1[i*3 + j]) + "\t");
             Console.WriteLine();
        }

        Console.WriteLine("c2:");
        for (int i = 0; i < 3; i++) { 
            for (int j = 0; j < 3; j++)   Console.Write((c2[i*3 + j]) + "\t");
             Console.WriteLine();
        }
       
        Console.WriteLine(count  +" intersections are: ");
        
        for(int i=0; i < count; i++){
            double x = (points[i*3] / points[i*3+2]);
            double y = (points[i * 3 + 1] / points[i * 3 + 2]);
             Console.WriteLine(x + "\t" + y);
        }
        
    }
}



}
