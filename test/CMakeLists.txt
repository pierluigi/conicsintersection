# brief help
# Once compiled you can run tests from the build folder
# > CTest -R RegexpName -C [Debug, Release]


#############TESTS 
find_package(Eigen REQUIRED CONFIG)
find_package(boost COMPONENTS unit_test_framework REQUIRED CONFIG)

file(GLOB_RECURSE testSrc *.cpp)
file(GLOB_RECURSE testHdr *.h)

#Run through each source
foreach(src ${testSrc})
        #Extract the filename without an extension (NAME_WE)
        get_filename_component(testName ${src} NAME_WE)

        #Add compile target
        add_executable(${testName} ${testHdr} ${src})

        #link to Boost libraries AND your targets and dependencies
		target_link_libraries(${testName} Boost::unit_test_framework )

		add_definitions(-DBOOST_TEST_MODULE=${testName})
        
		#move testing binaries into a testBin directory
        #set_target_properties(${testName} PROPERTIES RUNTIME_OUTPUT_DIRECTORY  ${CMAKE_BINARY_DIR}/tests)
		
		set_target_properties(${testName} PROPERTIES FOLDER tests)
        
		#Finally add it to test execution - 
        #Notice the WORKING_DIRECTORY and COMMAND
		MESSAGE(STATUS "add test ${testName}" )
        add_test(NAME ${testName} 
                 COMMAND ${testName} ${APP_TEST_DATA_PATH}/${PROJECT_NAME})
				 
endforeach(src)
	
