#include "conicsIntersection.h"

#include <iostream>
#include <boost/test/unit_test.hpp>
//#include <boost/test/floating_point_comparison.hpp>

using namespace Eigen;

#define CLOSE_TOL 1.0
#define CLOSE_TOL_P(x) boost::test_tools::percent_tolerance((x))


BOOST_AUTO_TEST_SUITE(ConicsIntersectionTest);

BOOST_AUTO_TEST_CASE(countIntersecion) {
    std::cout << "countIntersection test" << std::endl;
	ConicsIntersection<double> cint;
	ConicsIntersection<double>::Conic a, b;

	//make them proper conics
	a.setRandom();
	a = a * a.transpose().eval();

	//make them proper conics
	b.setRandom();
	b = b * b.transpose().eval();

	auto points = cint.intersect(a, b);
    auto points2 = cint.intersect(b, a);

    BOOST_REQUIRE_EQUAL(points.size(), points2.size());
    
    //intersections lay on the conics
    for (auto p : points){
        double ad = (p.transpose() * a * p);
        double bd = (p.transpose() * b * p);

        BOOST_REQUIRE_SMALL( ad, CLOSE_TOL);
        BOOST_REQUIRE_SMALL( bd, CLOSE_TOL );
    }
    
    //intersection is commutative
    for (auto p1 : points){
        bool present = false;
        for (auto p2 : points2){
            double x1z2 = p1.x()*p2.z();
            double x2z1 = p2.x()*p1.z();
            double y1z2 = p1.y()*p2.z();
            double y2z1 = p2.y()*p1.z();
            
            if (boost::test_tools::check_is_close(x1z2, x2z1, CLOSE_TOL_P(CLOSE_TOL)) &&
                boost::test_tools::check_is_close(y1z2, y2z1, CLOSE_TOL_P(CLOSE_TOL))){
                present = true;
                break;
            }
        }
        BOOST_REQUIRE(present);
    
    }
    
}

BOOST_AUTO_TEST_CASE(knownIntersection) {
  std::cout << "knownIntersection test" << std::endl;

  ConicsIntersection<double> cint;

  ConicsIntersection<double>::Conic c1;
  c1 << 1, 0, 0,
    0, 0, -0.5,
    0, -0.5, 0;
  ConicsIntersection<double>::Conic c2;
  c2 << 3, 0, 0,
    0, 0, -0.5,
    0, -0.5, -2;

  auto points = cint.intersect(c1, c2);

  BOOST_REQUIRE(points.size() == 2); //only two intersection

  for (auto p : points){
    double c1d = p.transpose() * c1 * p;
    double c2d = p.transpose() * c2 * p;
    BOOST_REQUIRE_SMALL(c1d, CLOSE_TOL);
    BOOST_REQUIRE_SMALL(c2d, CLOSE_TOL);

    BOOST_REQUIRE_CLOSE(p.y(), p.z(), CLOSE_TOL);
    BOOST_REQUIRE_CLOSE(std::abs(p.x()), std::abs(p.z()), CLOSE_TOL);
  }

}


BOOST_AUTO_TEST_CASE(noIntersection) {
  std::cout << "no intersection test" << std::endl;

  ConicsIntersection<double> cint;

  //build two circle centered in zero with different radius
  ConicsIntersection<double>::Conic c1;
  c1 << 1, 0, 0,
    0, 1, 0,
    0, 0, -1;
  ConicsIntersection<double>::Conic c2;
  c2 << 1, 0, 0,
    0, 1, 0,
    0, 0, -2;

  auto points = cint.intersect(c1, c2);

  BOOST_REQUIRE(points.size() == 0); //no intersections
}

BOOST_AUTO_TEST_SUITE_END();
